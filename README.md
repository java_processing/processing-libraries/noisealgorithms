# Quick presentation of the library

**/!\WARNING/!\\** this repository is the repo i'm working on, not the ready to use built library.

This library was created using [this template](https://github.com/processing/processing-library-template) and following the tutorial "[How to Make a Processing (Java) Library](https://www.youtube.com/watch?v=pI2gvl9sdtE)" by [the Coding Train](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw).

This library is meant to contain several noise algorithm implementations. It currently contains the following algorithms :
* OpenSimplexNoise (from 2D to 4D)
* Pseudo-Random Number Generator (**PRNG**)
    * Uniform distribution and normal distribution

For me, the overall goal is to learn how to make a library; and to learn how noise functions and pseudo-random number generation works. 

## Noise algorithms

### OpenSimplex noise

#### Quick presentation

The source code of the OpenSimplex noise algorithm is by Kurt Spencer and is available on his [github page](https://gist.github.com/KdotJPG/b1270127455a94ac5d19).

OpenSimplex noise is a "visually axis-decorrelated coherent noise algorithm based on the Simplectic honeycomb" (as mentioned on Kurt Spencer's github project).
It is implemented from 2 dimensions to 4 dimensions.

#### API
```java
// Create a OpenSimplexNoise object, optional argument for seed (default seed is 0)
OpenSimplexNoise noiseGenerator= new OpenSimplexNoise();

// Get a noise value
double xoff = 0.3;
double yoff = 2.4;
double val = noiseGenerator.eval(xoff, yoff);	// value between -1 and 1
```

### Pseudo-Random Number Generator

#### Quick presentation

A **PRNG** is an algorithm for generating a sequence of numbers whose properties approximate the properties of sequences of random numbers.
See this Wikipedia page ([Pseudorandom number generator](https://en.wikipedia.org/wiki/Pseudorandom_number_generator)) for more informations.

The PRNG used in this code is a **Linear Congruential Generator** (**LCG**).
A **LCG** is an algorithm that yields a sequence of pseudo-randomized numbers calculated with a discontinuous piecewise linear equation.
The variable is supposed to be a uniform random variable.

The generator is defined by this reccurent relation :
>![LCG](https://wikimedia.org/api/rest_v1/media/math/render/svg/3a40cd0032b03626a091a5a0e1b4684b3d5eb406)

Several values for a, c and m are possible. The one used in the code are:
* a = 6364136223846793005
* m = 2^64 for unsigned long (and 2^63 -1 for signed long, but this doesn't affect the generation of pseudo random number)
* c = 1442695040888963407

For more informations those Wikipedia pages ([Linear congruential generator](https://en.wikipedia.org/wiki/Linear_congruential_generator) and [Lehmer random number generator](https://en.wikipedia.org/wiki/Lehmer_random_number_generator#Parameters_in_common_use)).

For the normal random variable, the method used is the Marsaglia polar method.
See [this Wikipedia page](https://en.wikipedia.org/wiki/Marsaglia_polar_method) for more  informations.

#### API
```java
// Create a RandomGenerator object, optional argument for seed (int or long)
// default seed is 0
RandomGenerator rand = new RandomGenerator ();

// Get a new pseudorandom signed long between -m and m-1
long newNext = rand.next();

// Get the last long generated by next() (return the seed if next() hasn't been used yet)
long newCurrent = rand.current();


// Get a new pseudorandom int between -Integer.MAX_VALUE and Integer.MAX_VALUE (2^31 -1) (included)
//the next 5 functions also exist for short
int newInt = rand.nextInt();

// Get an int between Integer.MIN_VALUE (-2^31) and Integer.MAX_VALUE (2^31 -1) (included) using the current long value of the LCG
int crntInt = rand.currentInt();


// Get a new pseudorandom int between 0 and bound (excluded)
int bound = 50;
int newRangeInt = rand.nextInt (bound);

// Get an int between 0 and bound using the current long value of the LCG
int crntRangeInt = rand.currentInt (bound );

// Get a sequence of int of a given length, between 0 (included) and bound (excluded)
int length = 10;
int bound = 256;
int[] sequence = rand.nextIntSequence(length, bound);

// Get a new pseudorandom double between -1.0 and 1.0 (both values included)
//the 2 next functions also exist for float
double newDouble = rand.nextDouble();

// Get a double between -1.0 and 1.0 (both values included) using the current long value of the LCG
double crntDouble = rand.currentDouble();

// Same as creating a new RandomGenerator with sd as the seed
long newSeed = 10; // also works with an int
rand.setSeed(newSeed);

// Same as creating a new RandomGenerator with its initial seed
rand.reset();

// generates a standard normal random variable
double newGaussian = rand.nextGaussian();


// generates a normal random variable with mean 11.3 and standard deviation 4.2
double  mean = 11.3;
double std = 4.2;
double newGaussian = rand.nextGaussian(mean, std);
```
