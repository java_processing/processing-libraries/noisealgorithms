import noiseAlgorithms.OpenSimplexNoise;

OpenSimplexNoise osNoise = new OpenSimplexNoise();
PImage img;

void setup() {
  size(700, 500);
  colorMode(RGB, 255);
  
  img = createImage(width, height, RGB);
}

float anim = 0;
final float speed = 0.05;   // determine the speed of the animation
final float sharp = 1/50.;  // the greater sharp, the greater the gradient
float clr;

void draw() {
  img.loadPixels();
  
  for (int x=0 ; x<img.width ; x++) {
    for (int y=0 ; y<img.height; y++) {      
      clr = (float) osNoise.eval(sharp*x, sharp*y, anim);
      clr = map(clr, -1, 1, 0, 255);
      
      int pix = x + y * img.width;
      img.pixels[pix] = color(clr);
    }
  }
  img.updatePixels();
  image(img, 0, 0, width, height);
  
  anim+=speed;
}
