import noiseAlgorithms.OpenSimplexNoise;

OpenSimplexNoise osNoise = new OpenSimplexNoise();;


void setup() {
  frameRate(60);
  size(800, 500);
  strokeWeight(1.5);
  stroke(255);
  noFill();
}

final int nbPoints = 150;
final float increment = TWO_PI /((float) nbPoints);
final float radius = 150.;
float noise_radius;
final float amplitude_noise = 15.;
float x, y;

final float sharpness = 2.;
float anim = 0;
final float speed = 0.03;

void draw() {
  background(41);
  translate(width/2, height/2);
  scale(1, -1);
  
  beginShape();
  for (float angle=0 ; angle<=TWO_PI ; angle+=increment) {
    
    noise_radius = (float) osNoise.eval((double) cos(angle)*sharpness, (double) sin(angle)*sharpness, (double) anim);
    noise_radius = map(noise_radius, -1., 1., -amplitude_noise, amplitude_noise);
    
    x = (radius + noise_radius) * cos(angle);
    y = (radius + noise_radius) * sin(angle);
    
    vertex(x, y);
  }
  endShape(CLOSE);
  anim+=speed;
}
