import noiseAlgorithms.RandomGenerator;

public void setup() {
  size(500, 400);
}

int pix;
float clr;

long seed = 10;
RandomGenerator rand = new RandomGenerator(seed);
//RandomGenerator rand = new RandomGenerator();  // the default seed is 1
public void draw() {
  loadPixels();
  for (int y=0 ; y<height ; y++) {
    for (int x=0 ; x<width ; x++) {
      pix = x + y*width;
      int clr = rand.nextInt(256);
      pixels[pix] = color(clr);
    }
  }
  updatePixels();
  //rand.reset();  // reset the RandomGenerator rand to its initial state (as if it was re-initialized with its original seed)
  noLoop();
}
