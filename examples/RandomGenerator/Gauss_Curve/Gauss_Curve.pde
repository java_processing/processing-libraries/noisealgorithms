import noiseAlgorithms.RandomGenerator;


RandomGenerator rand = new RandomGenerator(2);
double std = 100.;  // standard deviation
int nb = 101;  // number of points of the graph
double dx;  // distance between two points
int[] h = new int[nb];  // store the absolute height of the points
float fps = Float.POSITIVE_INFINITY;  // frameRate

void setup() {
  frameRate(fps);
  size(800, 400);
  stroke(255);
  strokeWeight(2);
  noFill();
  
  dx = width/((float)nb);
  for (int i : h) {i=0;}  // initialize the points to 0;
}

int iter = 10;  // number of iteration per frame (you can try 1 / 10 / 100 / 1000)

void draw() {  

  double min, max;
  for (int n=0 ; n<iter ; n++) {
    double gs = rand.nextGaussian(width/2., std);  //  mean is width/2 (the graph is centered) / std = 100 (1/8th of the width)
    for (int i=0 ; i<nb ; i++) { // for each possible x-coordinate i*dx
      min = i*dx;
      max = min+dx;
      if (min<=gs && gs<max) {  // if gs is between i*dx and (i+1)*dx
        h[i]++;  // add one to the corresponding point
        i=nb;  // end the loop
      }
    }
  }
  
  background(41);
  translate(0, 3*height/4);
  scale(1, -1);
  int maxHeight = max(h);  // maximum absolute height 
  float x, y;  // height of the point
  beginShape(POINTS);
  for (int i=0 ; i<nb ; i++) {
    x = (float)((0.5+i)*dx);  // x coordinate
    y = map(h[i], 0, maxHeight, 0, 2*height/3.); // y coordinate
    push();
    vertex(x, y);
    pop();
  }
  endShape();
}
