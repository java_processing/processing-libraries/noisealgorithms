import noiseAlgorithms.RandomGenerator;

public void setup() {
  size(800, 500);
}


long seed = 1;
RandomGenerator rand = new RandomGenerator(seed);


float x;
float y;
int nbPoints;
float ampl = 190;

float squich;
float timeFreq = 0.06; // cycle per frame
float waveLength = 60.;  // in pixels
float squichAmpl = 20.;  // amplitude of the wave (in pixels)

public void draw() {
  background(41);
  translate(0 ,height/2);
  
  nbPoints = 100*10;
  
  beginShape(POINTS);
  noFill();
  stroke(255);
  strokeWeight(2);
  for(int i=0 ; i<nbPoints ; i++) {
    x = i*width/((float)nbPoints);
    squich = squichAmpl*sin(-frameCount*timeFreq + x/waveLength);
    y = rand.nextFloat();  // return a float between -1 and 1
    vertex(x, map(y, -1, 1, -ampl-squich, ampl+squich));
  }
  endShape();
  rand.reset();
}
